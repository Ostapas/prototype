﻿using UnityEngine;

public abstract class BallBase : MonoBehaviour
{

    [SerializeField] private float _speed;
    [SerializeField] private ParticleSystem _goalParticle;
    [SerializeField] private ParticleSystem _destroyParticle;

    protected void Move()
    {
        transform.Translate(Vector3.right * _speed);
    }

    private void Crash(ParticleSystem particle = null)
    {
        if (particle != null)
        {
            Instantiate(particle, transform.position, Quaternion.Euler(0, 0, 0));
        }
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Collector")
        {
            CollectorCollision(collision);
            Crash(_goalParticle);
        }
        else if (collision.gameObject.tag == "Bearing")
        {
            BearingCollision(collision);
            Crash(_destroyParticle);
        }
    }

    protected abstract void CollectorCollision(Collider2D collision);

    protected abstract void BearingCollision(Collider2D collision);
}