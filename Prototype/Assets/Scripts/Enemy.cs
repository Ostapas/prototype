﻿using UnityEngine;

public class Enemy : BallBase
{

    private void FixedUpdate()
    {
        Move();
    }

    protected override void BearingCollision(Collider2D collision)
    {
        collision.GetComponent<Bearing>().Damage();
    }

    protected override void CollectorCollision(Collider2D collision)
    {
        collision.GetComponentInParent<Bearing>().Goal();
    }
}