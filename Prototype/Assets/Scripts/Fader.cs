﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{

    [SerializeField] private float _time;
    private float a;

    void Awake()
    {
        a = 1;
    }

    void FixedUpdate()
    {
        if (gameObject.active)
        {
            a -= 1 / _time;
            GetComponent<Image>().color = new Color(0, 0, 0, a);
            if (a <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
