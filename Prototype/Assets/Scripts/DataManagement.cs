﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManagement : MonoBehaviour
{
    public static int GetInt(string name, int defaultValue = 0)
    {
        return PlayerPrefs.GetInt(name, defaultValue);
    }

    public static void SetInt(string name, int value)
    {
        PlayerPrefs.SetInt(name, value); 
        PlayerPrefs.Save();
    }

    public static float GetFloat(string name, float defaultValue = 0)
    {
        return PlayerPrefs.GetFloat(name, defaultValue);
    }

    public static void SetFloat(string name, float value)
    {
        PlayerPrefs.SetFloat(name, value); 
        PlayerPrefs.Save();
    }

    public static bool GetBool(string name, bool defaultValue = false)
    {
        if (PlayerPrefs.HasKey(name))
        {
            return PlayerPrefs.GetInt(name, 0) == 1;
        }
        else
        {
            return defaultValue;
        }
    }

    public static void SetBool(string name, bool value)
    {
        PlayerPrefs.SetInt(name, value ? 1 : 0); 
        PlayerPrefs.Save();
    }
}
