﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bearing : MonoBehaviour
{

    [SerializeField] private CanvasManagement _canvas;
    [SerializeField] private BallManagement _ballSpawner;
    [SerializeField] private SoundsManagement _sounds;

    [Space(10)]
    [SerializeField] private float _sensivityMultiplier;

    [Header("Animation")]
    [SerializeField] private SpriteRenderer _fader;

    [Header("Damage")]
    [SerializeField] private float _damageSpeed;

    [Header("Fail")]
    //[SerializeField] private float _failStartSize;
    [SerializeField] private float _failEndSize;
    [SerializeField] private float _failSpeed;
    [SerializeField] private float _failRotatingSpeed;

    private int _direction;
    private float _sensivity;
    private int _score;
    private int _hp;
    private Quaternion _startRotation;
    private Vector3 _startScale;

    void Start()
    {
        _startRotation = transform.rotation;
        _startScale = transform.localScale;
        _canvas.UpdateCoins();
        UpdateSensivity();
        Restart();
    }

    void FixedUpdate()
    {
        transform.Rotate(0, 0, _direction * _sensivity);
    }

    public void UpdateSensivity()
    {
        _sensivity = DataManagement.GetFloat("Sensivity", 1) * _sensivityMultiplier;
    }

    public void Rotate (int direction)
    {
        _direction = direction;
    }

    public void CollectCoin()
    {
        DataManagement.SetInt("Coins", DataManagement.GetInt("Coins", 0) + 1);
        _canvas.UpdateCoins();
    }

    public void Goal()
    {
        _score += 1;
        _canvas.UpdateCurrentScore(_score);
        _sounds.PlayGoal();
    }

    public void Damage()
    {
        _hp -= 1;
        _canvas.UpdateHP(_hp);
        if (_hp < 1)
        {
            Fail();
        }
        else
        {
            _fader.color = new Color(_fader.color.r, _fader.color.g, _fader.color.b, 1);
            StartCoroutine(DamageAnimation(_damageSpeed, _fader));
            _sounds.PlayDamage();
        }
    }

    private void Fail()
    {
        DataManagement.SetInt("LastScore", _score);
        if (_score > DataManagement.GetInt("BestScore"))
        {
            DataManagement.SetInt("BestScore", _score);
        }
        _canvas.Fail();
        _ballSpawner.Spawning(false);
        _sounds.PlayFail();
        _direction = 0;
        int direction = Random.Range(0, 2) == 0 ? -1 : 1;
        StartCoroutine(FailAnimation(transform.localScale.x, _failEndSize, _failSpeed, _failRotatingSpeed * direction, _fader));
        GetComponent<PolygonCollider2D>().enabled = false;
        GetComponentInChildren<CircleCollider2D>().enabled = false;
    }

    public void Restart()
    {
        _score = 0;
        _hp = 3;
        _canvas.UpdateCurrentScore(_score);
        _canvas.Restart();
        _ballSpawner.Restart();
        transform.rotation = _startRotation;
        transform.localScale = _startScale;
        StopAllCoroutines();
        //StartCoroutine(DamageAnimation(_damageSpeed, _fader));
        _fader.color = new Color(_fader.color.r, _fader.color.g, _fader.color.b, 0);
        GetComponent<PolygonCollider2D>().enabled = true;
        GetComponentInChildren<CircleCollider2D>().enabled = true;
    }

    IEnumerator DamageAnimation(float speed, SpriteRenderer fader)
    {
        for (float i = 1; i > 0; i -= speed * Time.deltaTime)
        {
            _fader.color = new Color(_fader.color.r, _fader.color.g, _fader.color.b, i);
            yield return null;
        }
    }

    IEnumerator FailAnimation(float startSize, float endSize, float speed, float rotatingSpeed, SpriteRenderer fader)
    {
        for (float i = startSize; i <= endSize; i += speed * Time.deltaTime)
        {
            if (fader.color.a < 1f)
            {
                fader.color = new Color(fader.color.r, fader.color.g, fader.color.b, fader.color.a + speed * Time.deltaTime);
            }
            transform.localScale = new Vector3(i, i, 1);
            transform.Rotate(0, 0, rotatingSpeed * Time.deltaTime);
            yield return null;
        }
    }
}