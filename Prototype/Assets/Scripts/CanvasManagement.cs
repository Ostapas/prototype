﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManagement : MonoBehaviour
{

    [SerializeField] private GameObject _fader;
    [Space(10)]
    [SerializeField] private Text _bestScore;
    [SerializeField] private Text _lastScore;
    [SerializeField] private Text _currentScore;
    [SerializeField] private Text[] _coins;
    [Space(10)]
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _shopMenu;
    [SerializeField] private GameObject _settingsMenu;
    [SerializeField] private GameObject _failMenu;
    [SerializeField] private GameObject _inGameMenu;
    [SerializeField] private GameObject _statusBar;
    [Space(10)]
    [SerializeField] private GameObject[] _hp;
    [Space(10)]
    [SerializeField] private AudioSource _tickSound;

    void Start()
    {
        _fader.SetActive(true);
        Restart();      
    }

    void FixedUpdate()
    {

    }

    public void OpenMenu(GameObject menu)
    {
        menu.SetActive(true);
    }

    public void CloseMenu(GameObject menu) 
    {
        menu.SetActive(false);
    }

    public void CloseAllMenus()
    {
        CloseMenu(_mainMenu);
        CloseMenu(_shopMenu);
        CloseMenu(_settingsMenu);
        CloseMenu(_failMenu);
        CloseMenu(_inGameMenu);
        CloseMenu(_statusBar);
    }

    public void UpdateScores()
    {
        _bestScore.text = DataManagement.GetInt("BestScore").ToString();
        _lastScore.text = DataManagement.GetInt("LastScore").ToString();
    }

    public void UpdateCurrentScore(int score)
    {
        _currentScore.text = score.ToString();
    }

    public void UpdateCoins()
    {
        foreach (Text _text in _coins)
        {
            _text.text = DataManagement.GetInt("Coins").ToString() + " $";
        }
    }

    public void UpdateHP(int hp)
    {
        foreach (GameObject obj in _hp)
        {
            obj.SetActive(false);
        }
        for (; hp > 0; hp--)
        {
            _hp[hp - 1].SetActive(true);
        }
    }

    public void Fail()
    {
        CloseAllMenus();
        OpenMenu(_failMenu);
        OpenMenu(_statusBar);

        UpdateScores();
    }

    public void Restart()
    {
        CloseAllMenus();
        OpenMenu(_mainMenu);

        UpdateScores();
        UpdateHP(3);
    }

    public void PlayTick()
    {
        _tickSound.Play();
    }
}
