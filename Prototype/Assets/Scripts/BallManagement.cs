﻿using UnityEngine;

public class BallManagement : MonoBehaviour
{

    [SerializeField] private float _spawnRadius;
    [SerializeField] private float _spawnStartDelay;
    [SerializeField] private float _spawnDelayMultiplier;
    [SerializeField] private float _spawnDelayMinimum;
    [Space(10)]
    [SerializeField] private GameObject[] _ball;

    private bool _playing;
    private float _timer;
    private float _spawnDelay;
    private Vector2 _targetVector;

    void Start()
    {
        _targetVector = new Vector2(transform.position.x, transform.position.y);
    }

    void FixedUpdate()
    {
        if (_playing)
        {
            if (_timer >= _spawnDelay)
            {
                Spawn();
                _timer = 0.0f;
                if (_spawnDelay > _spawnDelayMinimum)
                {
                    _spawnDelay -= _spawnDelayMultiplier;
                }
            }
            _timer += Time.deltaTime;
        }
    }

    private void Spawn()
    {
        int angle = Random.RandomRange(0, 36) * 10;
        Vector2 spawnPosition = (new Vector2(Mathf.Cos(angle * 0.01745f), Mathf.Sin(angle * 0.01745f)) * _spawnRadius) + _targetVector;
        Instantiate(_ball[Random.RandomRange(0, _ball.Length)], spawnPosition, Quaternion.Euler(0, 0, angle + 180));
    }

    public void Spawning(bool value)
    {
        _playing = value;
    }

    public void Restart()
    {
        _timer = _spawnDelay  = _spawnStartDelay;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(obj);
        }
    }
}