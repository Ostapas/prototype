﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManagement : MonoBehaviour
{

    [SerializeField] private AudioSource _goal;
    [SerializeField] private AudioSource _damage;
    [SerializeField] private AudioSource _fail;

    void Start()
    {

    }

    void Update()
    {

    }

    public void PlayGoal()
    {
        if (DataManagement.GetBool("Sounds", true))
        {
            _goal.Play();
        }
    }

    public void PlayDamage()
    {
        if (DataManagement.GetBool("Sounds", true))
        {
            _damage.Play();
        }
    }

    public void PlayFail()
    {
        if (DataManagement.GetBool("Sounds", true))
        {
            _fail.Play();
        }
    }
}
