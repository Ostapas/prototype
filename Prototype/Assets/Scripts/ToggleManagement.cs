﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleManagement : MonoBehaviour
{

    [SerializeField] private string _keyName;

    public void SetKey()
    {
        DataManagement.SetBool(_keyName, GetComponent<Toggle>().isOn);
    }

    public void UpdateToggle()
    {
        GetComponent<Toggle>().isOn = DataManagement.GetBool(_keyName, true);
    }
}