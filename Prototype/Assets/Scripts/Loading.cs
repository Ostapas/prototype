﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{

    [SerializeField] private GameObject _animationObject;

    private int _sceneID;

    void Start()
    {
        _sceneID = DataManagement.GetInt("Scene", 1);
        StartCoroutine(AsyncLoading());
    }

    IEnumerator AsyncLoading()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(_sceneID, LoadSceneMode.Single);
        while (!operation.isDone)
        {
            LoadingAnimation(_animationObject.transform);
            yield return null;
        }
    }

    private void LoadingAnimation(Transform target)
    {
        target.Rotate(0, 0, 5);
    }
}
