﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomManagment : MonoBehaviour
{

    [SerializeField] private CanvasManagement _canvas;
    [SerializeField] private SpriteRenderer _target;
    [SerializeField] private Skin[] _skins;

    void Start()
    {
        DataManagement.SetBool("Skin" + 0, true);
        for (int i = 0; i < _skins.Length; i++)
        {
            _skins[i].SetLockedPanelActive(!DataManagement.GetBool("Skin" + i));
        }
        UpdateEquippedSkin();
        _target.sprite = _skins[DataManagement.GetInt("Skin")].GetSprite();
    }

    void FixedUpdate()
    {
        
    }

    public void Click(int number)
    {
        if (DataManagement.GetBool("Skin" + number))
        {
            Equip(number);
        }
        else
        {
            if (DataManagement.GetInt("Coins") >= _skins[number].GetPrice())
            {
                Buy(number);
            }
        }
    }

    public void Buy(int number)
    {
        DataManagement.SetInt("Coins", DataManagement.GetInt("Coins") - _skins[number].GetPrice());
        _canvas.UpdateCoins();
        Unlock(number);
        Equip(number);
    }

    public void Unlock(int number)
    {
        DataManagement.SetBool("Skin" + number, true);
        _skins[number].SetLockedPanelActive(!DataManagement.GetBool("Skin" + number));
    }

    public void Equip(int number)
    {
        DataManagement.SetInt("Skin", number);
        _target.sprite = _skins[number].GetSprite();
        UpdateEquippedSkin();
    }

    private void UpdateEquippedSkin()
    {
        for (int i = 0; i < _skins.Length; i++)
        {
            _skins[i].SetEquippedPanelActive(false);
        }
        _skins[DataManagement.GetInt("Skin")].SetEquippedPanelActive(true);
    }
}
