﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skin : MonoBehaviour
{

    [SerializeField] private Sprite _sprite;
    [SerializeField] private int _price;
    [Space(10)]
    [SerializeField] private GameObject _lockedPanel;
    [SerializeField] private GameObject _equippedPanel;
    [SerializeField] private Text _priceText;
    [SerializeField] private Image _image;

    void Start()
    {
        _image.sprite = _sprite;
        _priceText.text = _price.ToString();
    }

    public void SetLockedPanelActive(bool value)
    {
        _lockedPanel.SetActive(value);
    }

    public void SetEquippedPanelActive(bool value)
    {
        _equippedPanel.SetActive(value);
    }

    public Sprite GetSprite()
    {
        return _sprite;
    }

    public int GetPrice()
    {
        return _price;
    }
}
