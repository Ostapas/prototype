﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManagement : MonoBehaviour
{

    [SerializeField] private string _keyName;
    [SerializeField] private Text _valueText;

    public void SetKey()
    {
        if (_valueText != null)
        {
            _valueText.text = GetComponent<Slider>().value.ToString("f2", new System.Globalization.CultureInfo("en-US", false));
        }
        DataManagement.SetFloat(_keyName, GetComponent<Slider>().value);
    }

    public void UpdateSlider()
    {
        if (_valueText != null)
        {
            _valueText.text = GetComponent<Slider>().value.ToString("f2", new System.Globalization.CultureInfo("en-US", false));
        }
        GetComponent<Slider>().value = DataManagement.GetFloat(_keyName, 1);
    }
}