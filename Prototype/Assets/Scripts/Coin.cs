﻿using UnityEngine;

public class Coin : BallBase
{

    [SerializeField] private float _rotationSpeed;
    [SerializeField] private Transform _icon;

    private void FixedUpdate()
    {
        Move();
        _icon.Rotate(0, 0, _rotationSpeed);
    }

    protected override void BearingCollision(Collider2D collision)
    {
        
    }

    protected override void CollectorCollision(Collider2D collision)
    {
        collision.GetComponentInParent<Bearing>().CollectCoin();
    }
}